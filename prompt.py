import openai

class PromptGeneration:
    def __init__(self, task, key='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'):
        self.task = task
        openai.api_key = key
        self.task_name = task['taskName']

    def get_prompts(self):
        # Parse Task Data
        prompt_form = '''작업 이름 as input
        
        Output response는 특정 작업을 수행할 때 필요한 질문 3~4개 생성

        Response format:
        1. 이 task를 수행하기 위한 하위작업 목록
        2. 이 task를 수행할 때 주의사항
        3. 이 task를 만들때 사용할 리소스 (기간 및 비용)
        '''
        sample_prompt = '''
        Task Name: 강원도 여행계획
        
        Generated prompts:
        1. 강원도 여행을 계획하기 위해서 어떤것들을 조사 및 준비해야 되나요?
        2. 강원도 여행을 계획하는데 알거나 주의해야 할 사항들은 무엇인가요?
        3. 계획하는데 소요되는 시간은 얼마나 걸리고, 필요한 자료들/리소스들은 무엇인가요?
        '''
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            stream=True,
            messages=[
                {"role": "system", "content": f"Given a task, will generate 3~4 useful follow up questions to complete the task. The response will be in Korean. A sample output is {sample_prompt}"},
                {"role": "user", "content": f'작업 이름: {self.task_name}'}
            ]
        )
        prompts = ""
        # return response['choices'][0]['message']['content']
        for chunk in response:
            # print(chunk)
            data = chunk['choices'][0]
            if data['finish_reason'] is not None:
                break
            prompts += data['delta']['content']
        return prompts

    def get_responses(self, prompts):
        response_form = '''작업 이름, prompts as input

        Prompts
        '''
        sample_response = '''
        Task Name: 토트넘 경기 표 예매
        Prompts:
        1. 토트넘 표를 예매하기 가장 효과적으로 할 수 있는 방법이 무엇인가요?
        2. 표 예매하는데 주의해야 될 사항들은 뭐가 있을까?
        3. 관람 계획을 하는데 예상 시간 및 필요한 자료들은 뭐가 있을까요?

        -Display Prompts Here-

        Output Responses:
        1. 토트넘 경기 티켓은 공식 웹사이트 (https://www.tottenhamhotspur.com)에서 경기 일정을 확인하고 구입 가능한 티켓이 있는지 검색해보는것입니다.
        만약 티켓이 없을 경우, 원가격보다 훨씬 높은 가격으로 제3자 판매업체에서 구매하는 것도 가능합니다.
        2. 표 예매를 할때에는 가격이나 연령제한, 구매기한 등을 잘 확인해야 됩니다. 
        특히 제3자 판매업체에서 구매하려는 경우, 안전하고 신뢰할 수 있는 페이지에서 구매하는것이 중요합니다.
        3. 티켓을 구매할때는 결제에 필요한 카드 및 개인정보가 필요하실 수도 있습니다. 추가적으로, 원활한 경기 관람 계획을 위해서는 경기 날짜에 맞는 비행기표 예매 및 숙박예약이 중요합니다.
        '''
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            stream=True,
            messages=[
                {"role": "system", "content": f"Given a task name and generated prompts, you will provide answers to the prompts to complete the task. The response will be in Korean, and be specific (e.g. provide specific places). A sample output is {sample_response}"},
                {"role": "user", "content": f'작업 이름: {self.task_name}, 연관질문: {prompts}'}
            ]
        )
        answer = ""
        for chunk in response:
            data = chunk['choices'][0]
            if data['finish_reason'] is not None:
                break
            answer += data['delta']['content']
        return answer
        # return response['choices'][0]['message']['content']
    
    def generate(self):
        prompts = self.get_prompts()
        responses = self.get_responses(prompts)
        return responses

    def get_report(textblob):
        pass

## Testing Area
sample_task = {'taskName': '영국 토트넘 맨유 경기 관람 여행 계획'}
test = PromptGeneration(sample_task)
result = test.generate()
print(result)